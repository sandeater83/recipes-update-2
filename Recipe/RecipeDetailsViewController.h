//
//  RecipeDetailsViewController.h
//  Recipe
//
//  Created by Stuart Adams on 1/28/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeDetailsViewController : UIViewController
{
    UILabel* title;
    UITextView* details;
    UIImageView* imageView;
}

@property (nonatomic, strong) NSDictionary* recipeDetailDictionary;

-(instancetype)initWithDictionary:(NSDictionary*)dict;
@end
