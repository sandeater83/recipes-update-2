//
//  RecipeDetailsViewController.m
//  Recipe
//
//  Created by Stuart Adams on 1/28/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "RecipeDetailsViewController.h"
#import "ViewController.h"

@interface RecipeDetailsViewController ()

@end

@implementation RecipeDetailsViewController

-(instancetype)initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    
    if(self)
    {
        self.recipeDetailDictionary = dict;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    int scrWidth = self.view.frame.size.width;
    int scrHeight = self.view.frame.size.height;
    
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, scrWidth, scrHeight)];
    imageView = [[UIImageView alloc]initWithFrame:CGRectMake(scrWidth/2 - 50, 20, 100, 100)];
    imageView.image = [UIImage imageNamed:_recipeDetailDictionary[@"recipeImgName"]];
    
    title = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, scrWidth, 100)];
    title.text = _recipeDetailDictionary[@"recipeName"];
    title.textAlignment = NSTextAlignmentCenter;
    
    details = [[UITextView alloc]initWithFrame:CGRectMake(10, 200, scrWidth - 20, scrHeight)];
    details.text = _recipeDetailDictionary[@"recipeText"];
    [details sizeToFit];
    details.editable = false;
    details.scrollEnabled = FALSE;
    details.textColor = [UIColor blackColor];
    
    int detailsHeight = details.frame.size.height;
    int titleHeight = title.frame.size.height;
    int imgHeight = imageView.frame.size.height;
    
    [self.view addSubview:scrollView];
    
    [scrollView setContentSize:CGSizeMake(scrWidth, detailsHeight + titleHeight + imgHeight + 50)];
    
    scrollView.backgroundColor = [UIColor redOrangeColor];
    
    [scrollView addSubview:title];
    [scrollView addSubview:imageView];
    [scrollView addSubview:details];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
