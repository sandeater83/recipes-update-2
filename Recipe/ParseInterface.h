 //
//  ParseInterface.h
//  Recipe
//
//  Created by Stuart Adams on 3/10/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface ParseInterface : NSObject
{
    NSArray* dataArray;
}

-(instancetype)initWithURL:(NSString*) URL
                    APIKey:(NSString*) apiKey
                     AppID:(NSString*) appId;


@property(nonatomic, retain) NSString* parseApiKey;
@property(nonatomic, retain) NSString* parseAppId;
@property(nonatomic, retain) NSString* parseApiUrl;

-(void)displayJSON;
-(void)displayJSON:(NSString*)jsonURL :(NSString*)searchForKey;
-(NSArray*)getAllObjectsFromParseClass;
-(void)getSingleObjectFromParseClass;
-(void)getSingleObjectFromParseClass:(NSString*)objectID;
-(void)postObjectToParseClass;
-(void)postObjectToParseClass:(NSDictionary*)requestBody;
-(void)updateObjectInParseClass;
-(void)updateObjectInParseClass:(NSString*)objectID
                               :(NSDictionary*)requestBody;
-(void)deleteObjectFromParseClass;
-(void)deleteObjectFromParseClass:(NSString*)objectID;

-(NSArray*)queryParse;

@end
