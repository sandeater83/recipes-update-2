//
//  ParseInterface.m
//  Recipe
//
//  Created by Stuart Adams on 3/10/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ParseInterface.h"

@implementation ParseInterface

-(instancetype)initWithURL:(NSString*) URL
                    APIKey:(NSString*) apiKey
                     AppID:(NSString*) appId
{
    self = [super init];
    if(self)
    {
        self.parseApiKey = apiKey;
        self.parseAppId = appId;
        self.parseApiUrl = URL;
    }
    return self;
}

-(NSMutableURLRequest*)createRequest:(NSString*)objectURL
{
    NSURL* url = [NSURL URLWithString: objectURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
    [request addValue:self.parseAppId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request addValue:self.parseApiKey forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    return request;
}

//DEL REQUEST
-(void)deleteObjectFromParseClass
{
    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/recipes/fvJySVYbhP"];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
    [request addValue:@"eG74ohJiijoqJZRn7uFXz1smTNG4TqDIWzyswDXb" forHTTPHeaderField:@"X-Parse-Application-Id"];
     [request addValue:@"qWNrPY5qGrn2RjefHAo0lTR3oOjLYZQcTthirjST" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setHTTPMethod:@"DELETE"];

    __autoreleasing NSURLResponse  *response = nil;

    NSError *errorObj;

    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];

    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];

    NSLog(@"%@", responseString);
}

-(void)deleteObjectFromParseClass:(NSString*)objectID
{
    NSString* objectURL = [NSString stringWithFormat:@"%@%@", self.parseApiUrl, objectID];
    
    NSMutableURLRequest* request = [self createRequest:objectURL];
    [request setHTTPMethod:@"DELETE"];
    
    __autoreleasing NSURLResponse  *response = nil;
    
    NSError *errorObj;
    
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
    
    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", responseString);
}


//PUT REQUEST
-(void)updateObjectInParseClass
{
    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/recipes/U93szQuj98"];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
    [request addValue:@"eG74ohJiijoqJZRn7uFXz1smTNG4TqDIWzyswDXb" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request addValue:@"qWNrPY5qGrn2RjefHAo0lTR3oOjLYZQcTthirjST" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setHTTPMethod:@"PUT"];

    //Adding fields to be updated
    NSDictionary* requestBody = @{@"recipeImgName":@"this changed"};

    if (requestBody) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestBody
                                    options:0
                                    error:&error];
        
        if (!error)
        {
            NSString* json = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
            NSMutableData *d = [NSMutableData dataWithData:[json dataUsingEncoding:NSUTF8StringEncoding]];
            [request setHTTPBody:d];
        }
    }


    __autoreleasing NSURLResponse  *response = nil;

    // Synchronous requests wrap the async call in a blocking thread. If
    // authentication fails, the connection attempts to continue. We lose
    // the error code at this point because instead of being "authentication
    // failed" it becomes "user cancelled auth request"
    NSError *errorObj;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];

    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];

    NSLog(@"%@", responseString);
}

-(void)updateObjectInParseClass:(NSString*)objectID
                               :(NSDictionary*)requestBody
{
    NSString* objectURL = [NSString stringWithFormat:@"%@%@", self.parseApiUrl, objectID];
    
    NSMutableURLRequest *request = [self createRequest:objectURL];
    [request setHTTPMethod:@"PUT"];
    
    if (requestBody) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestBody
                                                           options:0
                                                             error:&error];
        
        if (!error)
        {
            NSString* json = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSMutableData *d = [NSMutableData dataWithData:[json dataUsingEncoding:NSUTF8StringEncoding]];
            [request setHTTPBody:d];
        }
    }
    
    
    __autoreleasing NSURLResponse  *response = nil;
    
    // Synchronous requests wrap the async call in a blocking thread. If
    // authentication fails, the connection attempts to continue. We lose
    // the error code at this point because instead of being "authentication
    // failed" it becomes "user cancelled auth request"
    NSError *errorObj;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
    
    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
}

//POST REQUEST
-(void)postObjectToParseClass
{
    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/recipes/"];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
    [request addValue:@"eG74ohJiijoqJZRn7uFXz1smTNG4TqDIWzyswDXb" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request addValue:@"qWNrPY5qGrn2RjefHAo0lTR3oOjLYZQcTthirjST" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setHTTPMethod:@"POST"];

    //Setting up the request body in JSON format @"key":@"value"
    NSDictionary* requestBody = @{@"recipeImgName":@"blabla",
                                  @"recipeText":@"Recipe Text",
                                  @"recipeName":@"New Recipe"};

    //If the request body isn't empty
    if (requestBody)
    {
        //Create NSError object
        NSError *error;
        //Create data objext to store serialized jsonData
        //Select serilization options and pass dereferenced error object
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestBody
                                options:0
                                error:&error];

        if (!error)
        {
            //Creatinh JSON string
             NSString* json = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];

            //Creating changable dataobject
            //setting the data to the json string
            NSMutableData *d = [NSMutableData dataWithData:[json dataUsingEncoding:NSUTF8StringEncoding]];
            //Adding json data to requesst body
            [request setHTTPBody:d];
        }
    }


    __autoreleasing NSURLResponse  *response = nil;

    // Synchronous requests wrap the async call in a blocking thread. If
    // authentication fails, the connection attempts to continue. We lose
    // the error code at this point because instead of being "authentication
    // failed" it becomes "user cancelled auth request"
    NSError *errorObj;
    
    //sending POST request
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];

    //Getting response data
    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];

    NSLog(@"%@", responseString);
}

-(void)postObjectToParseClass:(NSDictionary*)requestBody
{
    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/recipes/"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
    [request addValue:@"eG74ohJiijoqJZRn7uFXz1smTNG4TqDIWzyswDXb" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request addValue:@"qWNrPY5qGrn2RjefHAo0lTR3oOjLYZQcTthirjST" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setHTTPMethod:@"POST"];
    
    //Setting up the request body in JSON format @"key":@"value"
//    NSDictionary* requestBody = @{@"recipeImgName":@"blabla",
//                                  @"recipeText":@"Recipe Text",
//                                  @"recipeName":@"New Recipe"};
    
    //If the request body isn't empty
    if (requestBody)
    {
        //Create NSError object
        NSError *error;
        //Create data objext to store serialized jsonData
        //Select serilization options and pass dereferenced error object
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestBody
                                                           options:0
                                                             error:&error];
        
        if (!error)
        {
            //Creatinh JSON string
            NSString* json = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            //Creating changable dataobject
            //setting the data to the json string
            NSMutableData *d = [NSMutableData dataWithData:[json dataUsingEncoding:NSUTF8StringEncoding]];
            //Adding json data to requesst body
            [request setHTTPBody:d];
        }
    }
    
    
    __autoreleasing NSURLResponse  *response = nil;
    
    // Synchronous requests wrap the async call in a blocking thread. If
    // authentication fails, the connection attempts to continue. We lose
    // the error code at this point because instead of being "authentication
    // failed" it becomes "user cancelled auth request"
    NSError *errorObj;
    
    //sending POST request
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
    
    //Getting response data
    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
}

//TODO: Generalize following methods
//GET ALL OBJECTS

/*-(void)getAllObjectsFromParseClass
{
    //Setting URL to the Parse API. Set to the recipes class
    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/recipes/"];
    
    //Setting up the request information
    //Adding URL to request, setting timeout interval in seconds
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
    //Adding headers and values to the request
    [request addValue:@"eG74ohJiijoqJZRn7uFXz1smTNG4TqDIWzyswDXb" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request addValue:@"qWNrPY5qGrn2RjefHAo0lTR3oOjLYZQcTthirjST" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    //Setting HTTP Method
    [request setHTTPMethod:@"GET"];
    
    //creating a response variable
    __autoreleasing NSURLResponse  *response = nil;
    
    //creating an error object
    NSError *errorObj;
    
    //Setting retrieved data to NSData object
    //Sending a synchronous request
    //Passing in dereferenced reponse variable to hold response codes
    //Passing in dereferenced errorObj variable to hold response codes
    NSData *data= [NSURLConnection sendSynchronousRequest:request
                    returningResponse:&response
                                error:&errorObj];
    
    //Converting NSData object into a string
    //Initializing if data bytes and data length
    //Encoded in UTF8
    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", responseString);
}*/

-(NSArray* )getAllObjectsFromParseClass
{
    NSMutableURLRequest *request = [self createRequest:self.parseApiUrl];
    //Setting HTTP Method
    [request setHTTPMethod:@"GET"];
    
    //creating a response variable
    __autoreleasing NSURLResponse  *response = nil;
    
    //creating an error object
    NSError *errorObj;
    
    //Setting retrieved data to NSData object
    //Sending a synchronous request
    //Passing in dereferenced reponse variable to hold response codes
    //Passing in dereferenced errorObj variable to hold response codes
    NSData *data= [NSURLConnection sendSynchronousRequest:request
                                        returningResponse:&response
                                                    error:&errorObj];
    
    //Converting NSData object into a string
    //Initializing if data bytes and data length
    //Encoded in UTF8
    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSData* jsonData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* jsonObject = [NSJSONSerialization
                           JSONObjectWithData:jsonData
                                        options:0
                                        error:nil];
    //NSLog(@"%@", jsonObject);
    NSMutableArray* dictionaryArray = [NSMutableArray new];
    for(NSDictionary* recipe in jsonObject[@"results"])
    {
        [dictionaryArray addObject:recipe];
    }
    
    return dictionaryArray;
}

//GET REQUEST
-(void)getSingleObjectFromParseClass
{
    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/recipes/iJxiqTKHU5"];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
    [request addValue:@"eG74ohJiijoqJZRn7uFXz1smTNG4TqDIWzyswDXb" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request addValue:@"qWNrPY5qGrn2RjefHAo0lTR3oOjLYZQcTthirjST" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setHTTPMethod:@"GET"];

    __autoreleasing NSURLResponse  *response = nil;

    NSError *errorObj;

    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];

    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];

    NSLog(@"%@", responseString);
}

-(void)getSingleObjectFromParseClass:(NSString*)objectID
{
    NSString* objectURL = [NSString stringWithFormat:@"%@%@", self.parseApiUrl, objectID];
    
    NSMutableURLRequest *request = [self createRequest:objectURL];
    [request setHTTPMethod:@"GET"];
    
    __autoreleasing NSURLResponse  *response = nil;
    
    NSError *errorObj;
    
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
    
    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", responseString);
}

//JSON Stuff
-(void)displayJSON:(NSString*)jsonURL :(NSString*)searchForKey
{
    //JSON File
    
    //Initializing NSURL variable with a URL string pointing to a JSON object
    NSURL* url = [NSURL URLWithString:jsonURL];
    
    //Initializing request with the URL from the above
    NSURLRequest* request = [[NSURLRequest alloc]initWithURL:url];
    
    //storing the raw data from the JSON URL request
    //Sending the request syncrounously
    //In this case we are not passing any objects in to store the response (IE HTTP 401) or Error
    NSData *theData = [NSURLConnection sendSynchronousRequest:request
                                            returningResponse:nil
                                                        error:nil];
    
    //Creating an array to store the JSON data
    //Serialization of the NSDATA object
    //Options are used for various read/write procedures
    //Error object used for storing eroor, none used in this case
    NSArray *newJSON = [NSJSONSerialization JSONObjectWithData:theData
                                                       options:0
                                                         error:nil];
    
    
    
    for (int i = 0; i < newJSON.count; i++) {
        NSDictionary* dictionary = [newJSON objectAtIndex:i];
        NSLog(@"%@", [dictionary objectForKey:searchForKey]);
    }
}


-(void)displayJSON
{
    [self displayJSON:@"http://aasquaredapps.com/TheMTPit/TheMTPITJSON.json" :@"organization"];
}



//Parse Query
-(NSArray*)queryParse
{
    //Creating a parse query object
    PFQuery *query = [PFQuery queryWithClassName:@"recipes"];
    
    NSMutableArray* dictionaryArray = [NSMutableArray new];
    //Executing query
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // Do something with the found objects
            for (PFObject *object in objects) {
               /* [self displayImageFromObject:object];

                LibraryBookObject* obj = [[LibraryBookObject alloc]initWithDictionary:object];
                NSLog(@"%@",obj.bookName);*/

                NSDictionary* dict = (NSDictionary*) object;
                NSMutableDictionary* recipeDetails = [NSMutableDictionary new];
                
                for(NSString* key in [dict allKeys])
                {
                    [recipeDetails setObject:[dict objectForKey:key]
                                      forKey:key];
                }
                
                [dictionaryArray addObject:recipeDetails];
            }

        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        [self setArray: dictionaryArray];
    }];
    
    //NSLog(@"Outside \n");
    //NSLog(@"%@", [self getArray]);
    return dictionaryArray;
}

-(void)setArray:(NSArray*)array
{
    dataArray = array;
}

-(NSArray*)getArray
{
    return dataArray;
}


@end
