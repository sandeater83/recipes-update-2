//
//  RecipeCollectionViewCell.m
//  Recipe
//
//  Created by Stuart Adams on 3/8/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "RecipeCollectionViewCell.h"

@implementation RecipeCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    {
        int cellWidth = self.frame.size.width;
        //int cellHeight = self.frame.size.height;
        
        self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 0, cellWidth - 10, cellWidth-10)];

        [self addSubview:self.imgView];
        
        self.cellLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, self.imgView.frame.origin.y + self.imgView.frame.size.height, cellWidth, 30)];
        self.cellLbl.textColor = [UIColor blackColor];
        self.cellLbl.font = [UIFont systemFontOfSize:12];
        self.cellLbl.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.cellLbl];
    }
    return self;
}

@end
