//
//  RecipeCollectionViewCell.h
//  Recipe
//
//  Created by Stuart Adams on 3/8/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) UIImageView* imgView;

@property (nonatomic, retain) UILabel* cellLbl;

@end
