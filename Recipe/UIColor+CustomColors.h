//
//  UIColor+CustomColors.h
//  Recipe
//
//  Created by Stuart Adams on 3/16/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+(UIColor*)peachColor;
+(UIColor*)redOrangeColor;
+(UIColor*)darkBrownColor;
@end
