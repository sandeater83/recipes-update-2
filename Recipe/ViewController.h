//
//  ViewController.h
//  Recipe
//
//  Created by Stuart Adams on 1/21/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "RecipeCollectionViewCell.h"
#import "RecipeDetailsViewController.h"
#import "ParseInterface.h"
#import "UIColor+CustomColors.h"

@interface ViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
{
    UICollectionView* recipeCollection;
    UICollectionViewFlowLayout* collectionFlow;
    NSArray* recipes;
}

@end

