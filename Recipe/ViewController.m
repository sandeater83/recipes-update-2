//
//  ViewController.m
//  Recipe
//
//  Created by Stuart Adams on 1/21/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
//    PFObject *testObject = [PFObject objectWithClassName:@"recipes"];
//    testObject[@"title"] = @"burgers";
//    [testObject saveInBackground];
    
    ParseInterface* pInterface = [[ParseInterface alloc]
            initWithURL:@"https://api.parse.com/1/classes/recipes/"
            APIKey:@"qWNrPY5qGrn2RjefHAo0lTR3oOjLYZQcTthirjST"
            AppID:@"eG74ohJiijoqJZRn7uFXz1smTNG4TqDIWzyswDXb"];
    
    recipes = [pInterface getAllObjectsFromParseClass];
    self.navigationItem.title = @"Recipes";
    
    //NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    
    //NSString* path = [[NSBundle mainBundle] pathForResource:@"RecipeList" ofType:@"plist"];
    
    //recipes = [[NSArray alloc]initWithContentsOfFile:path];
    //recipes = [pInterface queryParse];

    
   /* if(recipes.count > 0)
    {
        [ud setObject:recipes forKey:@"recipesDetails"];
        [ud synchronize];
    }
    else
    {
        recipes = [ud objectForKey:@"recipesDetails"];
    }*/

    collectionFlow = [UICollectionViewFlowLayout new];
    
    recipeCollection = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:collectionFlow];
    recipeCollection.backgroundColor = [UIColor peachColor];
    recipeCollection.delegate = self;
    recipeCollection.dataSource = self;
    [recipeCollection registerClass:[RecipeCollectionViewCell class]forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:recipeCollection];
    
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return recipes.count;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* indentifier = @"cell";
    NSDictionary* dict = [recipes objectAtIndex:indexPath.row];
    
    RecipeCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
    cell.cellLbl.text = dict[@"recipeName"];
    cell.imgView.image = [UIImage imageNamed:dict[@"recipeImgName"]];
    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    RecipeDetailsViewController* recipeVC = [[RecipeDetailsViewController alloc]initWithDictionary:[recipes objectAtIndex:indexPath.row]];
    
    [self.navigationController pushViewController:recipeVC animated:YES];
}

-(CGSize)collectionView:(UICollectionView*) collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/2 - 15, self.view.frame.size.width/2);
}

-(UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(20, 10, 10, 10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
