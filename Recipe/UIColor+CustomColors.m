//
//  UIColor+CustomColors.m
//  Recipe
//
//  Created by Stuart Adams on 3/16/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

/*

    peachColor:         #ffad61
    redOrangeColor:     #d45c37
    darkBrownColor:     #594c44
 
*/

+(UIColor*)peachColor
{
    return [UIColor colorWithRed:0.992 green:0.675 blue:0.408 alpha:.75];
}

+(UIColor*)redOrangeColor
{
    return [UIColor colorWithRed:0.82 green:0.357 blue:0.243 alpha:1];
}

+(UIColor*)darkBrownColor
{
    return [UIColor colorWithRed:0.349 green:0.294 blue:0.267 alpha:.1];
}

@end
